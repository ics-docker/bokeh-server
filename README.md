# bokeh-server docker image

Docker image to run a [Bokeh Server](https://bokeh.pydata.org/en/latest/docs/user_guide/server.html).


Docker pull command:

```
docker pull registry.esss.lu.se/ics-docker/bokeh-server:latest
```
