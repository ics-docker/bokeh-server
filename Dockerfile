FROM registry.esss.lu.se/ics-docker/miniconda:4.8.4

RUN conda create -n bokeh \
  python=3.7 \
  bokeh=2.2.1 \
  pandas=1.1.2 \
  scipy=1.5.2 \
  epics-base=7.0.4 \
  pyepics=3.4.3 \
  && conda clean -ay

ENV PYEPICS_LIBCA /opt/conda/envs/bokeh/base/lib/linux-x86_64/libca.so
ENV PATH /opt/conda/envs/bokeh/bin:$PATH

WORKDIR /apps

VOLUME /apps
EXPOSE 5006

CMD bokeh serve --address=0.0.0.0 --port 5006
